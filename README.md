# MoSBi webapp
Shiny webapp to execute biclustering algorithms and 
use the MoSBi ensemble approach.

For information about the algorithm please have a look at the publication:

>MoSBi: Automated signature mining for molecular stratification and subtyping
>
>Tim Daniel Rose, Thibault Bechtler, Octavia-Andreea Ciora, Kim Anh Lilian Le, Florian Molnar, Nikolai Koehler, Jan Baumbach, Richard Röttger, Josch Konstantin Pauling
>
>Proceedings of the National Academy of Sciences, 2022; 119 (16): e2118210119; 
>
>doi: [https://doi.org/10.1073/pnas.2118210119](https://doi.org/10.1073/pnas.2118210119)

Git repository of the mosbi R package can be found [here](https://gitlab.lrz.de/lipitum-projects/mosbi)


## Run in docker environment

**NOTE**: The user needs access to: `/var/run/docker.sock`

Run: `sudo docker-compose up`

The applications should now be available at [http://127.0.0.1:8082](http://127.0.0.1:8082)
