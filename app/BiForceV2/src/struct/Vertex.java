package struct;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Peng
 * this abstract class implements a 
 */
public class Vertex implements Comparable<Vertex> {
    //this indicate which set the vertex belongs to
    public int vtxset;
    public String value;
    public int vtxidx = 0;
    public double[] coords;
    public int clustnum=-1;
    
    public Vertex(String value, int level, int vtxidx)
    {
        this.value = value;
        this.vtxset = level;
        this.coords = null;
        this.vtxidx = vtxidx;
    }
    
    public Vertex(String value, int level, double[] coords, int vtxidx)
    {
        this.value = value;
        this.vtxset = level;
        this.coords = coords;
        this.vtxidx = vtxidx;
    }
    
    @Override
    public boolean equals(Object o)
    {
        //first check if the object is an instace of ActinoVertex
        if(!(o instanceof Vertex ))
            throw new IllegalArgumentException("this given object is not an instance of ActinoVertex");
        Vertex act = (Vertex)o;
        return (value.equals(act.value) &&
                (vtxset == act.vtxset));
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
    
    @Override
    public int compareTo(Vertex vtx)
    {
        //first check if the object is an instace of ActinoVertex
        if(!(vtx instanceof Vertex)) {
            throw new IllegalArgumentException("this given object is not an instance of ActinoVertex");
        }
        return (value.compareTo(vtx.value));
    }
    
    
    public int getIdx()
    {
        return vtxidx;
    }
    
    
}
