# Bi-Force

Biclustering approach published in:

_Sun, P., Speicher, N. K., Roettger, R., Guo, J., & Baumbach, J. (2014). Bi-Force: large-scale bicluster editing and its application to gene expression data biclustering. Nucleic acids research, gku201_

Software downloaded from:

[https://biclue.compbio.sdu.dk/](https://biclue.compbio.sdu.dk/)

