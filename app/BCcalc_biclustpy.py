import numpy as np
import biclustpy as bp
import sys

if __name__ == "__main__":
  
  print(sys.argv[1]) # Data
  print(sys.argv[2]) # Output
  print(sys.argv[3])
  
  algorithm = bp.Algorithm()
  
  algorithm.use_ch(alpha=float(sys.argv[3]))
  # algorithm.ilp_time_limit = 100
  # algorithm.ilp_tune = True
  
  weights = np.loadtxt(sys.argv[1])
  
  bi_clusters, obj_val, is_optimal = bp.compute_bi_clusters(weights, algorithm)
  
  bp.main.save_bi_clusters_as_xml(sys.argv[2], bi_clusters, obj_val, is_optimal, "Noname")
